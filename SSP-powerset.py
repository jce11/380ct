from random import randint, sample
from itertools import chain, combinations
import time
import numpy as np
from itertools import chain


class SSP():
    """Create subset incrance class

    Variables:
    S is an array of random numbers
    T is a integer of the target sum
    N is a integer of how many items in array S
    """
    def __init__(self, S=[], t=0):
        self.S = S
        self.t = t
        self.n = len(S)
        #
        self.decision = False
        self.total    = 0
        self.selected = []


    """print subset and target
    """
    def __repr__(self):
        return "SSP instance: S="+str(self.S)+"\tt="+str(self.t)

    """Create random subset
    n is passed in from user
    bitlength is set at 10 to limit size of numbers in subset

    max bit numner is calulated using formula 2**bitlength-1
    S array is created using a random generator between 0 and size of max bit lenth
        they are then sorted by smallest to largest
    T (target) is created by getting the sum from random sample of numbers in array S
    N is the amount of integers in subset S
    """
    def random_yes_instance(self, n, bitlength=10):
        max_n_bit_number = 2**bitlength-1
        self.S = sorted( [ randint(0,max_n_bit_number) for i in range(n) ] , reverse=True)
        self.t = sum( sample(self.S, randint(0,n)) )
        self.n = len( self.S )

    ###
    def powerset(self):
        candidate = []
        count = 0
        s = [[]]
        while count != self.n:
            for candidate in combinations(self.S, (count+1)):
                    s.append(candidate)
 #                   print(candidate)
            count += 1
        return s
                                


    """Try adding random subset to get target
    candidate array is initialized
    total is set to 0

    While the total does not equal the target
    candidate = Array of sample numbers from S between 0 and size of array
    total = sum of candidate

    """


    def try_at_random(self):
        candidate = []
        total = 0
        while total != self.t:
            candidate = sample(self.S, randint(0,self.n))
            total     = sum(candidate)
            print( "Trying: ", candidate, ", sum:", total )

    """Exhaustive search looks at all value combinations whos sum = target

    total: to store sum total
    count: count through combination amount 0 to Size of subset
    found: variable to return if the taget is found
    initialise candidate
    
    while total doesnt equal target
    while the count is less the size of subset
    get combinations of amount: 0 to size of subset
    
    total: set total equal to the sub of this combination
    if the total equals target
    found equals true and return found
    else count goes up by 1
    """

    def exh(self):
        candidate = []
        total = 0
        s = self.powerset()

        start = time.time()
        for list in s:
               # print( list )
                total = sum(list)
                print( "Trying: ", list, ", sum:", total )
                if total == self.t:
                    self.decision = True
                    end = time.time()
                    print(end-start)
                    return self.decision
           

    def builddynamic(self):
        candidate = []
        sett = []
        s = [[]]
        count = 0
        
        for number in self.S:
            if number <= self.t:
                sett.append(number)
        print("Dynamic set S=", sett)

        size = len(sett)
        while count != size:
            for candidate in combinations(sett, (count+1)):
                #print("THIS IS NUM: ", candidate)
                s.append(candidate)
 #                   print(candidate)
            count += 1
        return s

    def try_dynamic(self):
        candidate = []
        total = 0
        s = self.builddynamic()
        
        for list in s:
               # print( list )
                total = sum(list)
                print( "Trying: ", list, ", sum:", total )
                if total == self.t:
                    self.decision = True
                    return self.decision

    def try_dyn(self):
        candidate = []
        subset = [[]]
        np.full(self.t, self.n-1)

        
        for i in range(0, self.n):
            subset[i][0] = true;

        for i in range(1, (self.n +1)):
                       subset[i][0] = false;

        for i in range(1, self.t +1):
            for j in range(1, len(self.n + 1)):
                subset[i][j] = subset[i][j-1]
                if (i >= self.S[j-1]):
                    subset[i][j] = subset[i][j] | subset(i - self.S[j-1][j-1])

        for i in range(0, self.t):
            for j in range(0, self.n):
                print(subset[i][j])
                print("\n")

        return subset[self.t][self.n]
                                                         
            
        


instance = SSP()
instance.random_yes_instance(6)


print( instance )

#print( instance.powerset )

#instance.try_at_random()
#found = instance.try_exhaustive_search()
#print("Subset found: ", found)
#found = instance.try_dynamic()
print( instance.try_dyn() )
print("subset found: ", found)
