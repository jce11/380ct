from random import randint, sample
from itertools import chain, combinations
from time import time
from itertools import chain

class SSP():
    """Create subset incrance class

    Variables:
    S is an array of random numbers
    T is a integer of the target sum
    N is a integer of how many items in array S
    """
    def __init__(self, S=[], t=0):
        self.S = S
        self.t = t
        self.n = len(S)
        #
        self.decision = False
        self.total    = 0
        self.selected = []


    """print subset and target
    """
    def __repr__(self):
        return "SSP instance: S="+str(self.S)+"\tt="+str(self.t)

    """Create random subset
    n is passed in from user
    bitlength is set at 10 to limit size of numbers in subset

    max bit numner is calulated using formula 2**bitlength-1
    S array is created using a random generator between 0 and size of max bit lenth
        they are then sorted by smallest to largest
    T (target) is created by getting the sum from random sample of numbers in array S
    N is the amount of integers in subset S
    """
    def random_yes_instance(self, n, bitlength=5):
        max_n_bit_number = 2**bitlength-1
        self.S = sorted( [ randint(0,max_n_bit_number) for i in range(n) ] , reverse=True)
        self.t = sum( sample(self.S, randint(0,n)) )
        self.n = len( self.S )

    ###
    def powerset(self):
        candidate = []
        count = 0
        s = [[]]
        while count != self.n:
            for candidate in combinations(self.S, (count+1)):
                    s.append(candidate)
 #                   print(candidate)
            count += 1
        return s
                                


    """Try adding random subset to get target
    candidate array is initialized
    total is set to 0

    While the total does not equal the target
    candidate = Array of sample numbers from S between 0 and size of array
    total = sum of candidate

    """


    def try_at_random(self):
        candidate = []
        total = 0
        while total != self.t:
            candidate = sample(self.S, randint(0,self.n))
            total     = sum(candidate)
            print( "Trying: ", candidate, ", sum:", total )

    """Exhaustive search looks at all value combinations whos sum = target

    total: to store sum total
    count: count through combination amount 0 to Size of subset
    found: variable to return if the taget is found
    initialise candidate
    
    while total doesnt equal target
    while the count is less the size of subset
    get combinations of amount: 0 to size of subset
    
    total: set total equal to the sub of this combination
    if the total equals target
    found equals true and return found
    else count goes up by 1
    """

    def exh(self):
        candidate = []
        total = 0
        s = self.powerset()
        
        for list in s:
               # print( list )
                total = sum(list)
                print( "Trying: ", list, ", sum:", total )
                if total == self.t:
                    self.decision = True
                    return self.decision
           
            
        
    

    def try_exhaustive_search(self):
        total = 0
        count = 0
        candidate = []
        s = self.powerset()
        print(s)
        while total != self.t:
            while count < (self.n + 2):
                for candidate in combinations(self.S, count):
                    total = sum(candidate)
                    print( "Trying: ", candidate, ", sum:", total )
                    if total == self.t:
                        self.decision = True
                        return self.decision
                count+= 1


    


instance = SSP()
instance.random_yes_instance(6)


print( instance )

#print( instance.powerset )

#instance.try_at_random()
#found = instance.try_exhaustive_search()
#print("Subset found: ", found)
instance.exh()
